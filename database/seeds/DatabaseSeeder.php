<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Question;
use App\Answer;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

        /**factory(User::class, 5)->create()->each(function ($user){
            //this body will run for every user that has been created!

            
             create() --> This method creates a object and inserts into database
             make()   --> This method only creates a object.
            

            for($i=1; $i<=rand(5, 10); $i++){
                $user->questions()->create(factory(Question::class)->make()->toArray());
            }
        });*/

        factory(User::class, 5)->create()
            ->each(function($user){
                $user->questions()
                    ->saveMany(factory(Question::class, rand(2, 5))->make())
                    ->each(function($question){
                        $question->answers()->saveMany(factory(Answer::class, rand(2, 7))->make());
                    });
            });

            /**
             * Creating 5 Users and on that collection, each and its callback function is called.
             * Inside callback, $user's questions are created by saveMany() which return objects collection, on which we ran each and callback again, where $question's answers are created using saveMany.
             */
    }
}
